﻿using Flashcards.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Flashcards
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      filesList.ItemsSource = App.Model.GetAllCategories();
    }

    private void Test()
    {
      var data = new List<Card>()
      {
        new Card("question1", "answer1"),
        new Card("question2", "answer2"),
        new Card("question3", "answer3"),
        new Card("question4", "answer4")
      };

      var str = JsonConvert.SerializeObject(data); //convert to json string
      var test = JsonConvert.DeserializeObject<List<Card>>(str); //read

      using (var file = File.CreateText(@"D:\data.txt"))
      {
        (new JsonSerializer()).Serialize(file, data);
      }
    }

    private void SelectSet(string filePath)
    {
      cardViewer.FilePath = filePath;
    }

    private void btnSaveCard_Click(object sender, RoutedEventArgs e)
    {
      var topSideText = tbTopSide.Text;
      var bottomSideText = tbBottomSide.Text;
      var filePath = filesList.SelectedItem as String;

      var created = App.Model.CreateCard(filePath, topSideText, bottomSideText);

      if (!created)
        return;

      cardViewer.Update();
      tbTopSide.Text = null;
      tbBottomSide.Text = null;
    }

    private void filesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      SelectSet(filesList.SelectedItem as String);
    }

    private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
    {
      if (tbTopSide.IsFocused || tbBottomSide.IsFocused)
        return;

      e.Handled = cardViewer.OnKeyPressed(e.Key);
    }
  }
}
