﻿using Flashcards.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Navigation;

namespace Flashcards
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    private GodModel _model;

    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);

      _model = new GodModel();
      _model.Init();
    }

    protected override void OnActivated(EventArgs e)
    {
      base.OnActivated(e);
      //_model.Init();
    }

    protected override void OnDeactivated(EventArgs e)
    {
      base.OnDeactivated(e);
      //_model.SaveAll();
    }

    protected override void OnExit(ExitEventArgs e)
    {
      _model.SaveAll();

      base.OnExit(e);
    }

    public static GodModel Model => (App.Current as App)._model;
  }
}
