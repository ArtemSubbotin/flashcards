﻿using Flashcards.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Flashcards
{
  /// <summary>
  /// Interaction logic for CardViewer.xaml
  /// </summary>
  public partial class CategoryViewer : UserControl, INotifyPropertyChanged
  {
    public event PropertyChangedEventHandler PropertyChanged;

    private void Notify(String propName)
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
    }

    private List<Card> _cards;
    private int? _curIndex = null;

    public CategoryViewer()
    {
      InitializeComponent();
    }

    private Boolean _isAnswerShown = false;

    public Boolean IsAnswerShown
    {
      get { return _isAnswerShown; }
      set { _isAnswerShown = value; Notify(nameof(IsAnswerShown)); }
    }


    public String FilePath
    {
      get { return (String)GetValue(FilePathProperty); }
      set { SetValue(FilePathProperty, value); }
    }

    public static readonly DependencyProperty FilePathProperty =
        DependencyProperty.Register("FilePath", typeof(String), typeof(CategoryViewer), new PropertyMetadata(null, OnFilePathChanged));

    

    private static void OnFilePathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      (d as CategoryViewer).Update();
    }

    public void Update()
    {
      var cards = App.Model.LoadCards(FilePath);
      Update(cards, 0);
    }

    private void Update(List<Card> cards, Int32 cardIndex)
    {
      _cards = cards;
      _curIndex = 0;
      SetCard(GetIndex(cardIndex));
    }

    private void btnPrev_Click(object sender, RoutedEventArgs e)
    {
      Prev();
    }

    private void Prev()
    {
      SetCard(GetIndex(-1));
    }

    private void btnNext_Click(object sender, RoutedEventArgs e)
    {
      Next();
    }

    private void Next()
    {
      SetCard(GetIndex(+1));
    }

    private Int32 GetIndex(int inc)
    {
      if (_cards == null)
        return 0;

      if (_curIndex == null)
      {
        _curIndex = 0;
        return _curIndex.Value;
      }

      var nextIndex = _curIndex + inc;
      if (nextIndex < 0)
        nextIndex = _cards.Count - 1;
      else if (nextIndex >= _cards.Count)
        nextIndex = 0;

      _curIndex = nextIndex;

      return _curIndex.Value;
    }

    internal bool OnKeyPressed(Key key)
    {
      if (key == Key.Space || key == Key.Right)
      {
        if (IsAnswerShown)
        {
          Next();
        }
        else
        {
          IsAnswerShown = true;
        }
        return true;
      }
      else if (key == Key.Left)
      {
        Prev();
        return true;
      }
      else if (key == Key.Delete)
      {
        DeleteCurrentCard();
        return true;
      }

      return false;
    }

    private void SetCard(Int32 index)
    {
      if (_cards == null)
        return;

      _curIndex = index;
      cardToggle.DataContext = GetCard(index);
      IsAnswerShown = false;
    }

    private void DeleteCardButton_Click(object sender, RoutedEventArgs e)
    {
      DeleteCurrentCard();
    }

    private void DeleteCurrentCard()
    {
      var card = GetCard(_curIndex.Value);

      if (card != null)
      {
        App.Model.DeleteCard(FilePath, card);
        Update();
      }
    }

    private Card GetCard(int index)
    {
      if (_cards?.Count < index || _cards == null || _cards?.Count == 0)
        return null;

      return _cards[index];
    }
  }
}
