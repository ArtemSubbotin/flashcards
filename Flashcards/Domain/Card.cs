﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flashcards.Domain
{
  public class Card
  {
    public String TopSide { get; set; }
    public String BottomSide { get; set; }

    public String Hint { get; set; }

    public Boolean IsDeleted { get; set; } = false;

    public Card(String topSide, String bottomSide)
    {
      TopSide = topSide;
      BottomSide = bottomSide;
    }

    public override bool Equals(object obj)
    {
      var other = obj as Card;
      return this.TopSide == other.TopSide && this.BottomSide == other.BottomSide;
    }
  }
}
