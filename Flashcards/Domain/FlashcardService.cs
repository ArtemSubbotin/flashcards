﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flashcards.Domain
{
  public class FlashcardService
  {
    private static String SetsFolderName = "Categories";

    public List<String> LoadCategories()
    {
      var localDir = System.IO.Path.Combine(Directory.GetCurrentDirectory(), SetsFolderName);
      var files = Directory.GetFiles(localDir);
      return files.ToList();
    }

    public List<Card> ReadCards(String filePath)
    {
      var text = File.ReadAllText(filePath);
      return JsonConvert.DeserializeObject<List<Card>>(text);
    }

    internal void Save(String filePath, List<Card> cards)
    {
      var str = JsonConvert.SerializeObject(cards);
      File.WriteAllText(filePath, str);
    }
  }
}
