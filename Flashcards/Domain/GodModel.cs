﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flashcards.Domain
{
  public class GodModel
  {
    private CardCache _cache;

    public void Init()
    {
      _cache = new CardCache();
      _cache.Init();
    }

    public List<String> GetAllCategories()
    {
      return _cache.GetAllCategories();
    }

    public Boolean CreateCard(String filePath, String topSideText, String bottomSideText)
    {
      if (String.IsNullOrWhiteSpace(topSideText) || String.IsNullOrWhiteSpace(bottomSideText))
        return false;
      
      _cache.Add(filePath, new Card(topSideText, bottomSideText));

      return true;
    }

    public List<Card> LoadCards(String filePath)
    {
      return _cache.LoadCards(filePath);
    }

    internal List<Card> DeleteCard(string filePath, Card card)
    {
      return _cache.DeleteCard(filePath, card);
    }

    internal void SaveAll()
    {
      _cache.SaveAll();
    }
  }

  public class CardCache
  {
    private FlashcardService _flashcardService;
    private Dictionary<String, List<Card>> _categories;

    public void Init()
    {
      _flashcardService = new FlashcardService();
      _categories = new Dictionary<string, List<Card>>();
    }

    public List<String> GetAllCategories()
    {
      return _flashcardService.LoadCategories();
    }

    public List<Card> LoadCards(String filePath, Boolean includeDeleted = false)
    {
      var cards = GetCards(filePath);

      if (cards.Count == 0)
        SetCards(filePath, _flashcardService.ReadCards(filePath));

      return includeDeleted ? cards : cards.Where(x => x.IsDeleted == false).ToList();
    }

    public void Clear()
    {

    }

    internal void Add(string filePath, Card card)
    {
      GetCards(filePath).Add(card);
    }

    public List<Card> DeleteCard(string categoryFilePath, Card card)
    {
      card.IsDeleted = true;

      var cards = GetCards(categoryFilePath);
      return cards;
    }

    private List<Card> GetCards(string categoryPath)
    {
      if (_categories.ContainsKey(categoryPath) == false)
        _categories.Add(categoryPath, new List<Card>());

      return _categories[categoryPath];
    }

    private void SetCards(string categoryFilePath, List<Card> cards)
    {
      _categories[categoryFilePath].Clear();
      _categories[categoryFilePath].AddRange(cards);
    }

    internal void SaveAll()
    {
      foreach (var pair in _categories)
      {
        _flashcardService.Save(pair.Key, pair.Value);
      }
    }
  }
}
